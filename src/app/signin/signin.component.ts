import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/services/auth.service";
import {FormControl, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";
import {OrderService} from "../shared/services/order.service";
import {Store} from "@ngrx/store";
import {IOrder, IOrderDetails, IOrderValues, IVisaInfo, OrderStep} from "../shared/store/interfaces";
import {Observable} from "rxjs";
import {setStep} from "../shared/store/actions/order.step.actions";
import {take} from "rxjs/operators";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  userLogin: FormGroup
  errors: {} = {}
  public guestEmail
  public guestEmailError
  public orderStep: Observable<OrderStep>
  constructor(
    protected authService: AuthService,
    protected router: Router,
    private store: Store<{ orderStep: OrderStep }>
  ) {
    this.initForm()
    this.orderStep = store.select('orderStep')
  }

  ngOnInit(): void {
  }

  initForm() {
    this.userLogin = new FormGroup({
      'email' : new FormControl(''),
      'password' : new FormControl(''),
    })
  }

  submitForm() {
    this.errors = []
    this.authService.login(this.userLogin.value).subscribe((result) => {
      console.log(result)
      localStorage.setItem('token', result['token'])
      localStorage.setItem('email', this.userLogin.controls['email'].value)
      this.redirect()
    }, error => {
      this.errors = error.error
      console.log(error)
    })
  }

  submitFormGuest() {
    this.guestEmailError = null
    if (this.guestEmail) {
      localStorage.setItem('email', this.guestEmail)
      this.redirect()
    } else {
      this.guestEmailError = "Email must be filled"
    }
  }

  redirect() {
    this.orderStep.pipe(take(1)).subscribe((result) => {
      if (result.number > 1) {
        this.router.navigateByUrl('/order')
      } else {
        this.router.navigateByUrl('/')
      }
    }).unsubscribe()
  }

  hasError(control) {
    let hasError = this.errors[control];
    return {
      'form-error': hasError,
      'form-control-error': hasError,
    }
  }
}

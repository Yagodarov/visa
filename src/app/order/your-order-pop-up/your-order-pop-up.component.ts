import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {IOrder, IOrderDetails, OrderStep} from "../../shared/store/interfaces";
import {Observable} from "rxjs";

@Component({
  selector: 'app-your-order-pop-up',
  templateUrl: './your-order-pop-up.component.html',
  styleUrls: ['./your-order-pop-up.component.scss']
})
export class YourOrderPopUpComponent implements OnInit {

  public order: Observable<IOrder>
  constructor(
    private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails }>) {
    this.order = store.select('order')
  }

  ngOnInit(): void {
  }


  formatToDecimals(num) {
    return (Math.round(num * 100) / 100).toFixed(2);
  }
}

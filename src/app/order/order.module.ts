import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrderCountryModalComponent} from "./order-country-modal/order-country-modal.component";
import {OrderDetailsComponent} from "./order-steps/order-details/order-details.component";
import { OrderVisaComponent } from './order-steps/order-visa/order-visa.component';
import {RouterModule, Routes, ROUTES} from "@angular/router";
import {OrderComponent} from "./order.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatDialogModule} from "@angular/material/dialog";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import { MatMomentDateModule} from "@angular/material-moment-adapter";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {MatIconModule} from "@angular/material/icon";
import { OrderDeliveryComponent } from './order-steps/order-delivery/order-delivery.component';
import { YourOrderPopUpComponent } from './your-order-pop-up/your-order-pop-up.component';
import { OrderCheckoutComponent } from './order-steps/order-checkout/order-checkout.component';
import { OrderCheckoutFinalComponent } from './order-steps/order-checkout-final/order-checkout-final.component';
import {MatSelectModule} from "@angular/material/select";
import { OrderThankYouComponent } from './order-steps/order-thank-you/order-thank-you.component';
import {NgxFileDropModule} from "ngx-file-drop";
import {DndDirective} from "../shared/directives/dnd.directive";
import {AppModule} from "../app.module";
const routes: Routes = [
  {
    path: '',
    component: OrderComponent
  }
]

@NgModule({
  declarations: [
    OrderComponent,
    OrderCountryModalComponent,
    OrderDetailsComponent,
    OrderVisaComponent,
    OrderDeliveryComponent,
    YourOrderPopUpComponent,
    OrderCheckoutComponent,
    OrderCheckoutFinalComponent,
    OrderThankYouComponent,
    DndDirective,
  ],
  exports: [
    RouterModule
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    MatInputModule,
    MatButtonModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMomentDateModule,
    NgbModule,
    MatIconModule,
    MatSelectModule,
    NgxFileDropModule,
  ]
})
export class OrderModule { }

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Country} from "../../shared/models/country";
import {Store} from "@ngrx/store";
import {IOrder} from "../../shared/store/interfaces";
import {RootService} from "../../shared/services/root.service";
import {Nationality} from "../../shared/models/nationality";
import {Observable} from "rxjs";
import {setCountry, setNationality} from "../../shared/store/actions/order.actions";

@Component({
  selector: 'app-order-country-modal',
  templateUrl: './order-country-modal.component.html',
  styleUrls: ['./order-country-modal.component.scss']
})
export class OrderCountryModalComponent implements OnInit {
  public order: Observable<IOrder>
  public country: string
  public nationality: string
  public countries: Country[]
  public nationalities: Nationality[]
  constructor(
    private rootService: RootService,
    public dialogRef: MatDialogRef<OrderCountryModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private store: Store<{ order: IOrder }>) {
    this.order = store.select('order')
  }

  ngOnInit(): void {
    this.rootService.get().subscribe(
      result => {
        this.countries = result.countries.map((country) => {
          return Object.assign(new Country(), country)
        })
        this.nationalities = result.nationalities.map((nationality) => {
          return Object.assign(new Nationality(), nationality)
        })
        console.log(this);
      }
    )
    this.initValue()
  }

  initValue() {
    console.log('init')
    this.store.select("order").subscribe(res => {
      console.log(res)
      res.country ? this.country = res.country.name : null;
      res.nationality ? this.nationality = res.nationality.name : null;
    })
  }

  changeCountries() {
    const selectedCountry = this.countries.find((c) => c.name == this.country)
    const selectedNationality = this.nationalities.find((n) => n.name == this.nationality)
    console.log(selectedCountry)
    console.log(selectedNationality)
    this.store.dispatch(setCountry({country: selectedCountry}))
    this.store.dispatch(setNationality({nationality: selectedNationality}))
    this.dialogRef.close()
  }
}

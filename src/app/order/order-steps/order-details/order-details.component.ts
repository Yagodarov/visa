import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {
  IOrder,
  IOrderDetails,
  OrderStep,
  OrderDetails,
  IOrderValues,
  IVisaInfo
} from "../../../shared/store/interfaces";
import {Store} from "@ngrx/store";
import {setStep} from "../../../shared/store/actions/order.step.actions";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {VisaServicesService} from "../../../shared/services/visa-services.service";
import * as _moment from 'moment';
// @ts-ignore
import {default as _rollupMoment} from 'moment';

import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "@angular/material/core";
import {setOrderDetails} from "../../../shared/store/actions/order.details.actions";
import {OrderService} from "../../../shared/services/order.service";
import {Router} from "@angular/router";

const moment = _rollupMoment || _moment;

// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'YYYY',
  },
};

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['../../order.component.scss', './order-details.component.scss'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class OrderDetailsComponent implements OnInit {
  public errors = []
  public minDate = {
    year: 1950,
    month: 1,
    day: 1
  }
  public order: Observable<IOrder>
  public orderStep: Observable<OrderStep>
  public orderDetails: Observable<IOrderDetails>
  public orderValues: Observable<IOrderValues>
  public orderVisaInfo: Observable<IVisaInfo>
  public form: FormGroup

  constructor(
    private orderPriceService: VisaServicesService,
    private orderService: OrderService,
    protected router: Router,
    private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails, orderValues: IOrderValues, orderVisaInfo: IVisaInfo }>
  ) {
    this.orderStep = store.select('orderStep')
    this.order = store.select('order')
    this.orderDetails = store.select('orderDetails')
    this.orderValues = store.select('orderValues')
    this.orderVisaInfo = store.select('orderVisaInfo')
  }

  ngOnInit(): void {
    this.initFormGroup()
  }

  initFormGroup() {
    this.form = new FormGroup({
      name: new FormControl(''),
      surname: new FormControl(''),
      date: new FormControl(''),
      gender: new FormControl("male"),
      passport_number: new FormControl(''),
      date_passport_back: new FormControl(''),
      visa_start_date: new FormControl(''),
      visa_end_date: new FormControl(''),
      date_of_birth: new FormControl(''),
      passport_expiry_date: new FormControl(''),
      purpose_of_visit: new FormControl(''),
      main_destination: new FormControl(''),
      other_places: new FormControl(''),
      address_main_destination: new FormControl(''),
      job_title: new FormControl(''),
      trips_soon: new FormControl(''),
      company_name: new FormControl(''),
      company_address: new FormControl(''),
      position: new FormControl(''),
      email: new FormControl(localStorage.getItem('email'))
    });
    this.form.valueChanges.subscribe((value) => {
      this.setOrderDetailsValue(this.form.value)
    })
  }

  setOrderDetailsValue(values: OrderDetails) {
    this.store.dispatch(setOrderDetails({OrderDetails: values}))

    this.orderDetails.subscribe((orderDetails) => {
      console.log(orderDetails)
    });
  }

  previousStep() {
    this.store.dispatch(setStep({number: 1}))
  }

  nextStep() {
    this.store.dispatch(setStep({number: 3}))
  }

  submitForm(event) {
    if (event.type == 'click') {
        this.orderVisaInfo.subscribe((item) => {
          let values = Object.assign({}, this.form.value)
          values.date_passport_back = moment(values.date_passport_back).format("YYYY-MM-DD")
          values.visa_start_date = moment(values.visa_start_date).format("YYYY-MM-DD")
          values.visa_end_date = moment(values.visa_end_date).format("YYYY-MM-DD")
          values.passport_expiry_date = moment(values.passport_expiry_date).format("YYYY-MM-DD")
          values.date_of_birth = moment(values.date_of_birth).format("YYYY-MM-DD")
          values.first_name = values.name
          values.last_name = values.surname
          values['visa'] = item.id
          console.log(item, values)
          this.orderService.createOrder(values).subscribe((data) => {
            console.log(data)
            if (data.token) {
              localStorage.setItem('token', data.token)
            }
            this.store.dispatch(setOrderDetails({OrderDetails: data.order}) )
            this.nextStep()
          })
        })
    }
  }

  formatDates() {

  }
}

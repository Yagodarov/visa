import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {OrderService} from "../../../shared/services/order.service";
import {FormControl, FormGroup} from "@angular/forms";
import {setStep} from "../../../shared/store/actions/order.step.actions";
import {Store} from "@ngrx/store";
import {IOrder, IOrderDetails, OrderStep} from "../../../shared/store/interfaces";
import {Observable} from "rxjs";
import {FileSystemDirectoryEntry, FileSystemFileEntry, NgxFileDropEntry} from "ngx-file-drop";

@Component({
  selector: 'app-order-thank-you',
  templateUrl: './order-thank-you.component.html',
  styleUrls: ['../../order.component.scss', './order-thank-you.component.scss']
})
export class OrderThankYouComponent implements OnInit {
  @ViewChild("fileDropRef", { static: false }) fileDropEl: ElementRef;
  public order: Observable<IOrder>
  public form: FormGroup
  public files: any[] = []
  public uploadedDocuments = [
    {
      id: 1,
      name: 'bank_statement_1.pdf'
    },
    {
      id: 2,
      name: '1.pdf'
    },
    {
      id: 3,
      name: 'test.pdf'
    },
  ]
  constructor(private orderService: OrderService,
              private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails }>) {
    this.order = store.select('order')
  }

  ngOnInit(): void {
  }
  initFormGroup() {
    this.form = new FormGroup({
      name: new FormControl(''),
      process_information: new FormControl(''),
      marketing_emails: new FormControl(''),
      card: new FormGroup({
        'card_name': new FormControl(''),
        'card_number': new FormControl(''),
        'card_expire': new FormControl(''),
        'cvv': new FormControl(''),
      })
    });
    this.form.valueChanges.subscribe((value) => {
    })
  }

  uploadFile($event) {
    console.log($event)
    let files = $event.target.files; // outputs the first file
    let filesArray = Object.values(files)
    filesArray.forEach(file => {
      this.files.push(file)
    })
  }

  deleteDocument(i: number) {

  }

  printData()
  {
    let data = document.getElementById("printData").innerHTML
    let mywindow = window.open('', '_blank', 'height=400,width=600')
    mywindow.document.write('<html><head><title></title>')
    document.querySelectorAll('link, style').forEach(htmlElement => {
      mywindow.document.head.appendChild(htmlElement.cloneNode(true));
    });
    mywindow.document.write('</head><body >')
    mywindow.document.write(data)
    mywindow.document.write('</body></html>')

    mywindow.print();
    mywindow.close();

    return true;
  }

  public onFileDropped(files: {}) {// outputs the first file
    let filesArray = Object.values(files)
    filesArray.forEach(file => {
      this.files.push(file)
    })
  }

  public fileOver(event){
    console.log(event);
  }

  public fileLeave(event){
    console.log(event);
  }

  nextStep() {
    this.store.dispatch(setStep({number: 7}))
  }

  submitForm() {
    this.orderService.createOrder(this.form.value).subscribe((data) => {
      this.nextStep()
    })
  }
}

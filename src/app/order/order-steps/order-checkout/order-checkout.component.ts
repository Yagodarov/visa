import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {IOrder, IOrderDetails, OrderStep} from "../../../shared/store/interfaces";
import {FormControl, FormGroup} from "@angular/forms";
import {OrderService} from "../../../shared/services/order.service";
import {Store} from "@ngrx/store";
import {setStep} from "../../../shared/store/actions/order.step.actions";

@Component({
  selector: 'app-order-checkout',
  templateUrl: './order-checkout.component.html',
  styleUrls: ['../../order.component.scss', './order-checkout.component.scss']
})
export class OrderCheckoutComponent implements OnInit {
  public errors = []
  public selVisaApplicationType
  public selVisaSubmitType
  public visaApplicationTypes: any = [
    {
      id: 1,
      name: "Online with Visatogo",
      desc: "You will fill out the form at our website once you check out.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    },{
      id: 2,
      name: "Go to Embassy website",
      desc: "You will fill out the form at our office.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    },{
      id: 3,
      name: "Offline (by PDF)",
      desc: "You will need to go to Embassy website and fill out the application form.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    }
  ]
  public visaVisaSubmitTypes: any = [
    {
      id: 1,
      name: "At the Visatogo office",
      desc: "You will fill out the form at our website once you check out.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    },{
      id: 2,
      name: "At visa application centre",
      desc: "You will fill out the form at our office.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    },{
      id: 3,
      name: "At the Embassy",
      desc: "You will need to go to Embassy website and fill out the application form.",
      text: "Est tempore nulla nam suscipit ullam vel voluptas quia aut voluptatibus autem! Est consequatur facilis et consectetur ratione et rerum impedit et exercitationem nihil At nisi assumenda ex aspernatur nobis! Est labore consequatur ut ullam debitis hic perspiciatis perferendis est mollitia tempora est fugit voluptatem et sequi Quis et architecto voluptas. Id dolores minus et officiis aperiam in dignissimos reprehenderit sed voluptatem maiores eos consequatur sint? Non dolores ipsam et voluptatem nemo in nihil dolorem sed necessitatibus iure. \n" +
        "\n" +
        "Qui porro omnis ut placeat provident quo pariatur enim eum dolorum illo a iusto possimus eos officia omnis eum iste nesciunt. Aut excepturi molestiae sit impedit voluptatem aut numquam culpa non sint laudantium. Aut libero rerum non sunt doloribus vel totam minima."
    }
  ]
  public order: Observable<IOrder>
  public orderStep: Observable<OrderStep>
  public orderDetails: Observable<IOrderDetails>
  public form: FormGroup
  constructor(private orderService: OrderService,
              private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails }>) {
    this.orderStep = store.select('orderStep')
    this.order = store.select('order')
    this.orderDetails = store.select('orderDetails')
  }

  ngOnInit(): void {
  }

  initFormGroup() {
    this.form = new FormGroup({
      name: new FormControl(''),
    });
    this.form.valueChanges.subscribe((value) => {
    })
  }

  previousStep() {
    this.store.dispatch(setStep({number: 3}))
  }

  nextStep() {
    this.store.dispatch(setStep({number: 5}))
  }

  submitForm() {
    this.nextStep()
    // this.orderService.createOrder(this.form.value).subscribe((data) => {
    // })
  }
}

import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {IOrder, IOrderDetails, IOrderValues, OrderStep, OrderValues} from "../../../shared/store/interfaces";
import {FormControl, FormGroup} from "@angular/forms";
import {Store} from "@ngrx/store";
import {OrderService} from "../../../shared/services/order.service";
import {DeliveryRate} from "../../../shared/models/delivery-rate";
import {setStep} from "../../../shared/store/actions/order.step.actions";
import {setVisaServiceDelivery} from "../../../shared/store/actions/order.actions";

@Component({
  selector: 'app-order-delivery',
  templateUrl: './order-delivery.component.html',
  styleUrls: ['../../order.component.scss', './order-delivery.component.scss']
})
export class OrderDeliveryComponent implements OnInit {
  billing_address_different: false
  public errors: any[] = []
  public deliveryRates: DeliveryRate[]
  public order: Observable<IOrder>
  public orderStep: Observable<OrderStep>
  public orderDetails: Observable<IOrderDetails>
  public orderValues: Observable<IOrderValues>
  public form: FormGroup
  public countryCodes = [{"name":"AmericanSamoa","dial_code":"+1 684","code":"AS"},{"name":"Anguilla","dial_code":"+1 264","code":"AI"},{"name":"Bahamas","dial_code":"+1 242","code":"BS"},{"name":"Barbados","dial_code":"+1 246","code":"BB"},{"name":"Bermuda","dial_code":"+1 441","code":"BM"},{"name":"Dominica","dial_code":"+1 767","code":"DM"},{"name":"Dominican Republic","dial_code":"+1 849","code":"DO"},{"name":"Antigua and Barbuda","dial_code":"+1268","code":"AG"},{"name":"Canada","dial_code":"+1","code":"CA"},{"name":"Grenada","dial_code":"+1 473","code":"GD"},{"name":"Guam","dial_code":"+1 671","code":"GU"},{"name":"Jamaica","dial_code":"+1 876","code":"JM"},{"name":"Northern Mariana Islands","dial_code":"+1 670","code":"MP"},{"name":"Puerto Rico","dial_code":"+1 939","code":"PR"},{"name":"Saint Kitts and Nevis","dial_code":"+1 869","code":"KN"},{"name":"Montserrat","dial_code":"+1664","code":"MS"},{"name":"Saint Lucia","dial_code":"+1 758","code":"LC"},{"name":"Saint Vincent and the Grenadines","dial_code":"+1 784","code":"VC"},{"name":"Trinidad and Tobago","dial_code":"+1 868","code":"TT"},{"name":"Turks and Caicos Islands","dial_code":"+1 649","code":"TC"},{"name":"Virgin Islands, British","dial_code":"+1 284","code":"VG"},{"name":"Virgin Islands, U.S.","dial_code":"+1 340","code":"VI"},{"name":"United States","dial_code":"+1","code":"US"},{"name":"Algeria","dial_code":"+213","code":"DZ"},{"name":"Benin","dial_code":"+229","code":"BJ"},{"name":"Burkina Faso","dial_code":"+226","code":"BF"},{"name":"Cote d'Ivoire","dial_code":"+225","code":"CI"},{"name":"Cameroon","dial_code":"+237","code":"CM"},{"name":"Cape Verde","dial_code":"+238","code":"CV"},{"name":"Central African Republic","dial_code":"+236","code":"CF"},{"name":"Chad","dial_code":"+235","code":"TD"},{"name":"Angola","dial_code":"+244","code":"AO"},{"name":"British Indian Ocean Territory","dial_code":"+246","code":"IO"},{"name":"Congo","dial_code":"+242","code":"CG"},{"name":"Congo, The Democratic Republic of the Congo","dial_code":"+243","code":"CD"},{"name":"Burundi","dial_code":"+257","code":"BI"},{"name":"Djibouti","dial_code":"+253","code":"DJ"},{"name":"Botswana","dial_code":"+267","code":"BW"},{"name":"Comoros","dial_code":"+269","code":"KM"},{"name":"Aruba","dial_code":"+297","code":"AW"},{"name":"Morocco","dial_code":"+212","code":"MA"},{"name":"Mali","dial_code":"+223","code":"ML"},{"name":"Mauritania","dial_code":"+222","code":"MR"},{"name":"Niger","dial_code":"+227","code":"NE"},{"name":"Mauritius","dial_code":"+230","code":"MU"},{"name":"Nigeria","dial_code":"+234","code":"NG"},{"name":"Mozambique","dial_code":"+258","code":"MZ"},{"name":"Rwanda","dial_code":"+250","code":"RW"},{"name":"Madagascar","dial_code":"+261","code":"MG"},{"name":"Malawi","dial_code":"+265","code":"MW"},{"name":"Mayotte","dial_code":"+262","code":"YT"},{"name":"Namibia","dial_code":"+264","code":"NA"},{"name":"Reunion","dial_code":"+262","code":"RE"},{"name":"Egypt","dial_code":"+20","code":"EG"},{"name":"Libyan Arab Jamahiriya","dial_code":"+218","code":"LY"},{"name":"Tunisia","dial_code":"+216","code":"TN"},{"name":"Gambia","dial_code":"+220","code":"GM"},{"name":"Guinea","dial_code":"+224","code":"GN"},{"name":"Senegal","dial_code":"+221","code":"SN"},{"name":"Togo","dial_code":"+228","code":"TG"},{"name":"Ghana","dial_code":"+233","code":"GH"},{"name":"Liberia","dial_code":"+231","code":"LR"},{"name":"Sao Tome and Principe","dial_code":"+239","code":"ST"},{"name":"Sierra Leone","dial_code":"+232","code":"SL"},{"name":"Equatorial Guinea","dial_code":"+240","code":"GQ"},{"name":"Gabon","dial_code":"+241","code":"GA"},{"name":"Guinea-Bissau","dial_code":"+245","code":"GW"},{"name":"Seychelles","dial_code":"+248","code":"SC"},{"name":"Sudan","dial_code":"+249","code":"SD"},{"name":"Ethiopia","dial_code":"+251","code":"ET"},{"name":"Kenya","dial_code":"+254","code":"KE"},{"name":"Somalia","dial_code":"+252","code":"SO"},{"name":"Tanzania, United Republic of Tanzania","dial_code":"+255","code":"TZ"},{"name":"Uganda","dial_code":"+256","code":"UG"},{"name":"Lesotho","dial_code":"+266","code":"LS"},{"name":"Swaziland","dial_code":"+268","code":"SZ"},{"name":"Zambia","dial_code":"+260","code":"ZM"},{"name":"Zimbabwe","dial_code":"+263","code":"ZW"},{"name":"South Africa","dial_code":"+27","code":"ZA"},{"name":"Eritrea","dial_code":"+291","code":"ER"},{"name":"Faroe Islands","dial_code":"+298","code":"FO"},{"name":"Greenland","dial_code":"+299","code":"GL"},{"name":"Saint Helena, Ascension and Tristan Da Cunha","dial_code":"+290","code":"SH"},{"name":"Cayman Islands","dial_code":"+345","code":"KY"},{"name":"Belgium","dial_code":"+32","code":"BE"},{"name":"Aland Islands","dial_code":"+358","code":"AX"},{"name":"Albania","dial_code":"+355","code":"AL"},{"name":"Bulgaria","dial_code":"+359","code":"BG"},{"name":"Cyprus","dial_code":"+357","code":"CY"},{"name":"Andorra","dial_code":"+376","code":"AD"},{"name":"Armenia","dial_code":"+374","code":"AM"},{"name":"Belarus","dial_code":"+375","code":"BY"},{"name":"Bosnia and Herzegovina","dial_code":"+387","code":"BA"},{"name":"Croatia","dial_code":"+385","code":"HR"},{"name":"Netherlands","dial_code":"+31","code":"NL"},{"name":"Luxembourg","dial_code":"+352","code":"LU"},{"name":"Malta","dial_code":"+356","code":"MT"},{"name":"Portugal","dial_code":"+351","code":"PT"},{"name":"Lithuania","dial_code":"+370","code":"LT"},{"name":"Moldova","dial_code":"+373","code":"MD"},{"name":"Monaco","dial_code":"+377","code":"MC"},{"name":"Macedonia","dial_code":"+389","code":"MK"},{"name":"Montenegro","dial_code":"+382","code":"ME"},{"name":"Greece","dial_code":"+30","code":"GR"},{"name":"France","dial_code":"+33","code":"FR"},{"name":"Spain","dial_code":"+34","code":"ES"},{"name":"Finland","dial_code":"+358","code":"FI"},{"name":"Gibraltar","dial_code":"+350","code":"GI"},{"name":"Iceland","dial_code":"+354","code":"IS"},{"name":"Ireland","dial_code":"+353","code":"IE"},{"name":"Hungary","dial_code":"+36","code":"HU"},{"name":"Estonia","dial_code":"+372","code":"EE"},{"name":"Holy See (Vatican City State)","dial_code":"+379","code":"VA"},{"name":"Latvia","dial_code":"+371","code":"LV"},{"name":"San Marino","dial_code":"+378","code":"SM"},{"name":"Serbia","dial_code":"+381","code":"RS"},{"name":"Slovenia","dial_code":"+386","code":"SI"},{"name":"Ukraine","dial_code":"+380","code":"UA"},{"name":"Italy","dial_code":"+39","code":"IT"},{"name":"Czech Republic","dial_code":"+420","code":"CZ"},{"name":"Austria","dial_code":"+43","code":"AT"},{"name":"Denmark","dial_code":"+45","code":"DK"},{"name":"Romania","dial_code":"+40","code":"RO"},{"name":"Liechtenstein","dial_code":"+423","code":"LI"},{"name":"Norway","dial_code":"+47","code":"NO"},{"name":"Poland","dial_code":"+48","code":"PL"},{"name":"Switzerland","dial_code":"+41","code":"CH"},{"name":"Slovakia","dial_code":"+421","code":"SK"},{"name":"Guernsey","dial_code":"+44","code":"GG"},{"name":"Isle of Man","dial_code":"+44","code":"IM"},{"name":"Jersey","dial_code":"+44","code":"JE"},{"name":"United Kingdom","dial_code":"+44","code":"GB"},{"name":"Sweden","dial_code":"+46","code":"SE"},{"name":"Svalbard and Jan Mayen","dial_code":"+47","code":"SJ"},{"name":"Germany","dial_code":"+49","code":"DE"},{"name":"Belize","dial_code":"+501","code":"BZ"},{"name":"Costa Rica","dial_code":"+506","code":"CR"},{"name":"Cuba","dial_code":"+53","code":"CU"},{"name":"Argentina","dial_code":"+54","code":"AR"},{"name":"Brazil","dial_code":"+55","code":"BR"},{"name":"Chile","dial_code":"+56","code":"CL"},{"name":"Colombia","dial_code":"+57","code":"CO"},{"name":"Bolivia, Plurinational State of","dial_code":"+591","code":"BO"},{"name":"Nicaragua","dial_code":"+505","code":"NI"},{"name":"Panama","dial_code":"+507","code":"PA"},{"name":"Peru","dial_code":"+51","code":"PE"},{"name":"Mexico","dial_code":"+52","code":"MX"},{"name":"Martinique","dial_code":"+596","code":"MQ"},{"name":"Netherlands Antilles","dial_code":"+599","code":"AN"},{"name":"Paraguay","dial_code":"+595","code":"PY"},{"name":"El Salvador","dial_code":"+503","code":"SV"},{"name":"Falkland Islands (Malvinas)","dial_code":"+500","code":"FK"},{"name":"Guatemala","dial_code":"+502","code":"GT"},{"name":"Haiti","dial_code":"+509","code":"HT"},{"name":"Honduras","dial_code":"+504","code":"HN"},{"name":"Saint Pierre and Miquelon","dial_code":"+508","code":"PM"},{"name":"South Georgia and the South Sandwich Islands","dial_code":"+500","code":"GS"},{"name":"Venezuela, Bolivarian Republic of Venezuela","dial_code":"+58","code":"VE"},{"name":"Ecuador","dial_code":"+593","code":"EC"},{"name":"French Guiana","dial_code":"+594","code":"GF"},{"name":"Guadeloupe","dial_code":"+590","code":"GP"},{"name":"Guyana","dial_code":"+595","code":"GY"},{"name":"Saint Barthelemy","dial_code":"+590","code":"BL"},{"name":"Saint Martin","dial_code":"+590","code":"MF"},{"name":"Suriname","dial_code":"+597","code":"SR"},{"name":"Uruguay","dial_code":"+598","code":"UY"},{"name":"Australia","dial_code":"+61","code":"AU"},{"name":"Christmas Island","dial_code":"+61","code":"CX"},{"name":"Cocos (Keeling) Islands","dial_code":"+61","code":"CC"},{"name":"Antarctica","dial_code":"+672","code":"AQ"},{"name":"Brunei Darussalam","dial_code":"+673","code":"BN"},{"name":"Cook Islands","dial_code":"+682","code":"CK"},{"name":"Malaysia","dial_code":"+60","code":"MY"},{"name":"Philippines","dial_code":"+63","code":"PH"},{"name":"New Zealand","dial_code":"+64","code":"NZ"},{"name":"Nauru","dial_code":"+674","code":"NR"},{"name":"Norfolk Island","dial_code":"+672","code":"NF"},{"name":"Papua New Guinea","dial_code":"+675","code":"PG"},{"name":"New Caledonia","dial_code":"+687","code":"NC"},{"name":"Niue","dial_code":"+683","code":"NU"},{"name":"Palau","dial_code":"+680","code":"PW"},{"name":"Marshall Islands","dial_code":"+692","code":"MH"},{"name":"Micronesia, Federated States of Micronesia","dial_code":"+691","code":"FM"},{"name":"Indonesia","dial_code":"+62","code":"ID"},{"name":"Singapore","dial_code":"+65","code":"SG"},{"name":"Thailand","dial_code":"+66","code":"TH"},{"name":"Fiji","dial_code":"+679","code":"FJ"},{"name":"Solomon Islands","dial_code":"+677","code":"SB"},{"name":"Timor-Leste","dial_code":"+670","code":"TL"},{"name":"Tonga","dial_code":"+676","code":"TO"},{"name":"Vanuatu","dial_code":"+678","code":"VU"},{"name":"French Polynesia","dial_code":"+689","code":"PF"},{"name":"Kiribati","dial_code":"+686","code":"KI"},{"name":"Samoa","dial_code":"+685","code":"WS"},{"name":"Tuvalu","dial_code":"+688","code":"TV"},{"name":"Wallis and Futuna","dial_code":"+681","code":"WF"},{"name":"Tokelau","dial_code":"+690","code":"TK"},{"name":"Kazakhstan","dial_code":"+7 7","code":"KZ"},{"name":"Russia","dial_code":"+7","code":"RU"},{"name":"Cambodia","dial_code":"+855","code":"KH"},{"name":"China","dial_code":"+86","code":"CN"},{"name":"Bangladesh","dial_code":"+880","code":"BD"},{"name":"Macao","dial_code":"+853","code":"MO"},{"name":"Pitcairn","dial_code":"+872","code":"PN"},{"name":"Japan","dial_code":"+81","code":"JP"},{"name":"Korea, Republic of South Korea","dial_code":"+82","code":"KR"},{"name":"Vietnam","dial_code":"+84","code":"VN"},{"name":"Hong Kong","dial_code":"+852","code":"HK"},{"name":"Korea, Democratic People's Republic of Korea","dial_code":"+850","code":"KP"},{"name":"Laos","dial_code":"+856","code":"LA"},{"name":"Taiwan","dial_code":"+886","code":"TW"},{"name":"Afghanistan","dial_code":"+93","code":"AF"},{"name":"Bahrain","dial_code":"+973","code":"BH"},{"name":"Bhutan","dial_code":"+975","code":"BT"},{"name":"Azerbaijan","dial_code":"+994","code":"AZ"},{"name":"Pakistan","dial_code":"+92","code":"PK"},{"name":"Myanmar","dial_code":"+95","code":"MM"},{"name":"Maldives","dial_code":"+960","code":"MV"},{"name":"Oman","dial_code":"+968","code":"OM"},{"name":"Mongolia","dial_code":"+976","code":"MN"},{"name":"Nepal","dial_code":"+977","code":"NP"},{"name":"Palestinian Territory, Occupied","dial_code":"+970","code":"PS"},{"name":"Qatar","dial_code":"+974","code":"QA"},{"name":"Turkey","dial_code":"+90","code":"TR"},{"name":"India","dial_code":"+91","code":"IN"},{"name":"Sri Lanka","dial_code":"+94","code":"LK"},{"name":"Iraq","dial_code":"+964","code":"IQ"},{"name":"Jordan","dial_code":"+962","code":"JO"},{"name":"Kuwait","dial_code":"+965","code":"KW"},{"name":"Lebanon","dial_code":"+961","code":"LB"},{"name":"Saudi Arabia","dial_code":"+966","code":"SA"},{"name":"Syrian Arab Republic","dial_code":"+963","code":"SY"},{"name":"Yemen","dial_code":"+967","code":"YE"},{"name":"Israel","dial_code":"+972","code":"IL"},{"name":"United Arab Emirates","dial_code":"+971","code":"AE"},{"name":"Iran, Islamic Republic of Persian Gulf","dial_code":"+98","code":"IR"},{"name":"Georgia","dial_code":"+995","code":"GE"},{"name":"Kyrgyzstan","dial_code":"+996","code":"KG"},{"name":"Tajikistan","dial_code":"+992","code":"TJ"},{"name":"Turkmenistan","dial_code":"+993","code":"TM"},{"name":"Uzbekistan","dial_code":"+998","code":"UZ"}]
  public selCountryCode
  public selAddress2
  public selAddress3
  public selBillingAddress2
  public selBillingAddress3
  constructor(
    private orderService: OrderService,
    private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails, orderValues: OrderValues }>) {
    this.order = store.select('order')
    this.orderStep = store.select('orderStep')
    this.orderValues = store.select('orderValues')
    this.orderDetails = store.select('orderDetails')
  }

  ngOnInit(): void {
    this.initFormGroup()
    this.getDeliveryType()
  }

  initFormGroup() {
    this.form = new FormGroup({
      delivery: new FormGroup({
        delivery_type: new FormControl(''),
        delivery_rate: new FormControl(''),
        first_name: new FormControl(''),
        last_name: new FormControl(''),
        country: new FormControl(''),
        city: new FormControl(''),
        company: new FormControl(''),
        address: new FormControl(''),
        post_code: new FormControl(''),
        invoice: new FormControl(false),
        order: new FormControl(''),
        phone: new FormControl(''),
      }),
      invoice: new FormGroup({
        city: new FormControl(''),
        company: new FormControl(''),
        address: new FormControl(''),
        post_code: new FormControl(''),
      })
    });
    this.orderDetails.subscribe((res) => {
      this.form.controls['delivery']['controls']['order'].setValue(res.id)
    })
    this.form.valueChanges.subscribe((value) => {
      let deliveryRateValue = this.form.controls['delivery']['controls']['delivery_rate']['value']
      if (deliveryRateValue) {
        let deliveryRate = this.deliveryRates.find((rate) => {
          return deliveryRateValue == rate.id
        })
        this.store.dispatch(setVisaServiceDelivery({delivery: deliveryRate.price}))
      }
    })
  }

  getDeliveryType() {
    this.store.select('order').subscribe((order) => {
      this.orderService.checkDeliveryType({
        country: order.country.name
      }).subscribe((data) => {
        this.form.controls['delivery']['controls']['delivery_type'].setValue(data[0].id)
        this.orderService.checkDeliveryRate({
          delivery_type: data[0].id
        }).subscribe((data) => {
          this.deliveryRates = data
        })
      })
    }).unsubscribe()
  }

  setCountryCode() {
  }

  getFormValues() {
    let values = this.form.value
    values.delivery.phone = this.selCountryCode + values.delivery.phone
    values.delivery.address = [values.delivery.address, this.selAddress2, this.selAddress3].join(" ")
    values.invoice.address = [values.invoice.address, this.selBillingAddress2, this.selBillingAddress3].join(" ")
    return values
  }

  submitForm() {
    let values = this.getFormValues()
    this.orderService.createDelivery(values).subscribe((res) => {
      console.log(res)
      this.store.dispatch(setStep({number: 4}))
    }, (err) => {
      console.log(err)
    })
  }
}

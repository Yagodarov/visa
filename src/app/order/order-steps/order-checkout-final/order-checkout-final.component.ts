import { Component, OnInit } from '@angular/core';
import {Observable} from "rxjs";
import {IOrder, IOrderDetails, OrderStep} from "../../../shared/store/interfaces";
import {FormControl, FormGroup} from "@angular/forms";
import {OrderService} from "../../../shared/services/order.service";
import {Store} from "@ngrx/store";
import {setStep} from "../../../shared/store/actions/order.step.actions";

@Component({
  selector: 'app-order-checkout-final',
  templateUrl: './order-checkout-final.component.html',
  styleUrls: ['../../order.component.scss', './order-checkout-final.component.scss']
})
export class OrderCheckoutFinalComponent implements OnInit {
  public errors = []
  public paymentTypes = [
    {
      id: 1,
      name: "Online by card"
    },
    {
      id: 2,
      name: "Other payment methods"
    }
  ]
  public selVisaApplicationType
  public selVisaSubmitType
  public selPaymentType
  public order: Observable<IOrder>
  public orderStep: Observable<OrderStep>
  public orderDetails: Observable<IOrderDetails>
  public form: FormGroup
  constructor(private orderService: OrderService,
              private store: Store<{ order: IOrder, orderStep: OrderStep, orderDetails: IOrderDetails }>) {
    this.orderStep = store.select('orderStep')
    this.order = store.select('order')
    this.orderDetails = store.select('orderDetails')
  }

  ngOnInit(): void {
    this.initFormGroup()
  }

  initFormGroup() {
    this.form = new FormGroup({
      name: new FormControl(''),
      process_information: new FormControl(''),
      marketing_emails: new FormControl(''),
      card: new FormGroup({
        'card_name': new FormControl(''),
        'card_number': new FormControl(''),
        'card_expire': new FormControl(''),
        'cvv': new FormControl(''),
      })
    });
    this.form.valueChanges.subscribe((value) => {
    })
  }

  previousStep() {
    this.store.dispatch(setStep({number: 4}))
  }

  nextStep() {
    this.store.dispatch(setStep({number: 6}))
  }

  setPaymentType(paymentType) {
    this.selPaymentType = paymentType
  }

  submitForm() {
      this.nextStep()
  }
}

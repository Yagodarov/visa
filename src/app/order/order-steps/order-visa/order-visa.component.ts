import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {IOrder, OrderStep} from "../../../shared/store/interfaces";
import {Country} from "../../../shared/models/country";
import {Nationality} from "../../../shared/models/nationality";
import {VisaType} from "../../../shared/models/visa-type";
import {VisaCategory} from "../../../shared/models/visa-category";
import {VisaServiceType} from "../../../shared/models/visa-service-type";
import {VisaService} from "../../../shared/models/visa-service";
import {RootService} from "../../../shared/services/root.service";
import {ActivatedRoute, Router} from "@angular/router";
import {VisaTypeService} from "../../../shared/services/visa-type.service";
import {VisaCategoriesService} from "../../../shared/services/visa-categories.service";
import {VisaServiceTypesService} from "../../../shared/services/visa-service-types.service";
import {VisaServicesService} from "../../../shared/services/visa-services.service";
import {
  setCountry,
  setNationality, setOrderValues, setVisaCategory, setVisaService, setVisaServiceType,
  setVisaType
} from "../../../shared/store/actions/order.actions";
import {Store} from "@ngrx/store";
import {MatDialog} from "@angular/material/dialog";
import {OrderCountryModalComponent} from "../../order-country-modal/order-country-modal.component";
import {setStep} from "../../../shared/store/actions/order.step.actions";
import {OrderService} from "../../../shared/services/order.service";
import {setVisaInfo} from "../../../shared/store/actions/order.visa.actions";

@Component({
  selector: 'app-order-visa',
  templateUrl: './order-visa.component.html',
  styleUrls: ['../../order.component.scss', './order-visa.component.scss']
})
export class OrderVisaComponent implements OnInit, AfterViewInit {
  public order: Observable<IOrder>
  public orderStep: Observable<OrderStep>
  public errors: any[]
  public urlParams: any
  public countries: Country[]
  public nationalities: Nationality[]
  public visaTypes: VisaType[]
  public visaCategories: VisaCategory[]
  public visaServicesTypes: VisaServiceType[]
  public visaServices: VisaService[]

  public selCountry: Country
  public selDestination: Country
  public selNationality: Nationality
  public selVisaType: VisaType
  public selVisaCategory: VisaCategory
  public selVisaServiceType: VisaServiceType
  public selVisaService: VisaService

  constructor(
    private rootService: RootService,
    private route: ActivatedRoute,
    private orderService: OrderService,
    private visaTypeService: VisaTypeService,
    private visaCategoriesService: VisaCategoriesService,
    private visaServiceTypesService: VisaServiceTypesService,
    private visaServicesService: VisaServicesService,
    private store: Store<{ order: IOrder, orderStep: OrderStep }>,
    protected router: Router,
    public dialog: MatDialog
  ) {
    this.order = store.select('order')
    this.orderStep = store.select('orderStep')

  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.urlParams = params
    });
    this.getCountriesAndNationalities()
  }

  ngAfterViewInit() {
  }

  getCountriesAndNationalities() {
    this.rootService.get().subscribe(
      result => {
        this.countries = result.countries
        this.nationalities = result.nationalities
        // this.selCountry = this.countries.find(country => country.name == this.urlParams['country'])
        // this.selNationality = this.nationalities.find(nationality => nationality.name == this.urlParams['nationality'])

        this.initOnStateChange()
      }
    )
  }

  getVisaTypes() {
    // this.setDeparture()
    // this.setDestination()
    this.visaTypes = []
    this.visaCategories = []
    this.visaServicesTypes = []
    this.visaServices = []
    this.visaTypeService.get(this.selCountry.name, this.selNationality.name).subscribe(
      result => {
        this.visaTypes = result
      }
    )
  }

  selectVisaType(visaType: VisaType) {
    this.selVisaType = visaType
    this.selVisaCategory = null
    this.selVisaServiceType = null
    this.selVisaService = null
    this.setVisaType(visaType)
    this.getVisaCategories()
  }

  getVisaCategories() {
    this.visaCategories = []
    this.visaServicesTypes = []
    this.visaServices = []
    this.visaCategoriesService.get(this.selCountry.name, this.selNationality.name, this.selVisaType.name).subscribe(
      result => {
        this.visaCategories = result
      }
    )
  }

  selectVisaCategory(visaCategory: VisaCategory) {
    this.selVisaCategory = visaCategory
    this.selVisaServiceType = null
    this.selVisaService = null
    this.getTypesOfServices()
    this.setVisaCategory(visaCategory)
  }

  getTypesOfServices() {
    this.visaServicesTypes = []
    this.visaServices = []
    this.visaServiceTypesService.get(this.selCountry.name, this.selNationality.name, this.selVisaType.name).subscribe(
      result => {
        this.visaServicesTypes = result
      }
    )
  }

  selectTypeOfServices(visaServiceType: VisaServiceType) {
    this.selVisaServiceType = visaServiceType
    this.selVisaService = null
    this.getVisaServices()
    this.setVisaServiceType(visaServiceType)
  }

  getVisaServices() {
    this.visaServices = []
    this.visaServicesService.get(this.selCountry.name, this.selNationality.name, this.selVisaType.name, this.selVisaServiceType.name).subscribe(
      result => {
        this.visaServices = result
      }
    )
  }

  selectVisaService(visaService: VisaService) {
    this.selVisaService = visaService
    this.setVisaService(visaService)
  }

  initOnStateChange() {
    this.store.select('order').subscribe((res: IOrder) => {
      console.log(res)
      if (this.selCountry == res.country &&
        this.selNationality == res.nationality)
        return;
      this.selCountry = res.country
      this.selNationality = res.nationality
      this.selVisaType = res.visa_type
      this.selVisaServiceType = res.visa_service_type
      this.selVisaCategory = res.visa_category
      this.selVisaService = res.visa_service
      console.log(this)
      this.getVisaTypes()
      this.getVisaCategories()
      this.getTypesOfServices()
      this.getVisaServices()
    })
  }

  setNationality(nationality: Nationality) {
    this.store.dispatch(setNationality({nationality: nationality}));
  }

  setCountry(country: Country) {
    this.store.dispatch(setCountry({country: country}));
  }

  setVisaType(visaType) {
    this.store.dispatch(setVisaType({visa_type: visaType}));
  }

  setVisaService(visaService) {
    this.store.dispatch(setVisaService({visa_service: visaService}));
  }

  setVisaCategory(visaCategory) {
    this.store.dispatch(setVisaCategory({visa_category: visaCategory}));
  }

  setVisaServiceType(visaServiceType) {
    this.store.dispatch(setVisaServiceType({visa_service_type: visaServiceType}));
  }

  openModal() {
    const dialogRef = this.dialog.open(OrderCountryModalComponent, {
      maxWidth: '985px',
      panelClass: 'custom-modal'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.initOnStateChange()
    });
  }

  createOrder() {
    return this.orderService.createVisa({
      country: this.selCountry.id,
      nationality: this.selNationality.id,
      visa_type: this.selVisaType.id,
      visa_category: this.selVisaCategory.id,
      type_of_service: this.selVisaServiceType.id,
      service: this.selVisaService.id
    })
  }

  submitForm() {
    this.createOrder().subscribe((result) => {
      this.nextStep(result)
    }, error => {
      this.errors = error.error;
    });
  }

  nextStep(result) {
    this.store.dispatch(setVisaInfo({VisaInfo: result}))
    if (localStorage.getItem('email')) {
      this.store.dispatch(setStep({number: 2}))
    } else {
      this.router.navigateByUrl('/auth').then((result) => {
        this.store.dispatch(setStep({number: 2}))
      })
    }
  }
}

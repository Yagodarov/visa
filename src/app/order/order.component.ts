import { Component, OnInit } from '@angular/core';
import {Store} from "@ngrx/store";
import {IOrder, OrderStep} from "../shared/store/interfaces";
import {Observable} from "rxjs";
import {setStep} from "../shared/store/actions/order.step.actions";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {
  step = null;
  public orderStep: Observable<OrderStep>
  constructor(
    private route: ActivatedRoute,
    private store: Store<{orderStep: OrderStep }>,) {
    this.orderStep = store.select('orderStep')
    this.orderStep.subscribe((res) => {
      this.step = res.number
    })
    this.route.queryParams.subscribe((res) => {
      this.store.dispatch(setStep({number: res.step}))
    })
  }

  ngOnInit(): void {
    this.initOnStateChange()
  }

  initOnStateChange() {
    this.store.select('orderStep').subscribe((res) => {
      this.step = res.number
    })
  }
}

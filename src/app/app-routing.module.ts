import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainpageComponent } from './mainpage/mainpage.component';
import {SigninComponent} from "./signin/signin.component";
import {VisaComponent} from "./visa/visa.component";

const routes: Routes = [
  { path: '', component:  MainpageComponent},
  { path: 'order', loadChildren: () =>
      import('./order/order.module').then(m => m.OrderModule),
  },
  { path: 'auth', component:  SigninComponent},
  { path: 'profile', loadChildren: () =>
      import('./profile/profile.module').then(m => m.ProfileModule),
  },
  { path: 'visas', component:  VisaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

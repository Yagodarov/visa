import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VisaType } from '../models/visa-type';

@Injectable({
  providedIn: 'root'
})
export class VisaTypeService {

  constructor(private httpClient: HttpClient) { }

  
  get(country: string, nationality: string): Observable<VisaType[]> {
    const headers = new  HttpHeaders()
    .set("country", country)
    .set('nationality', nationality)
    return this.httpClient.get<VisaType[]>(`check-visa-type`, {headers: headers})
  }

}

import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {IOrderDelivery, OrderDetails, OrderValues, OrderValuesPost} from "../store/interfaces";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private httpClient: HttpClient) { }

  createVisa(data: OrderValuesPost): Observable<OrderValues> {
    return this.httpClient.post<OrderValues>('order/visa/', data)
  }

  createOrder(data: OrderDetails): Observable<{
    order: OrderDetails,
    token: string
  }> {
    return this.httpClient.post<{ order: OrderDetails, token: string }>('order/create/', data)
  }

  createDelivery(data: IOrderDelivery): Observable<any> {
    return this.httpClient.post<{ any }>('order/delivery/', data)
  }

  checkDeliveryType(data: { country: string }): Observable<any> {
    const headers = new  HttpHeaders()
      .set("country", data.country)
    return this.httpClient.get<any>('check-delivery-type/', {headers: headers})
  }

  checkDeliveryRate(data: { delivery_type: string }): Observable<any> {
    const headers = new  HttpHeaders()
      .set("delivery-type", data.delivery_type + '')
    return this.httpClient.get<OrderValues>('check-delivery-rate/', {headers: headers})
  }
}

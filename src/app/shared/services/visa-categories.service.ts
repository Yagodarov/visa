import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VisaCategory } from '../models/visa-category';

@Injectable({
  providedIn: 'root'
})
export class VisaCategoriesService {

  constructor(private httpClient: HttpClient) { }

  get(country: string, nationality: string, type: string): Observable<VisaCategory[]> {
    const headers = new  HttpHeaders()
    .set("country", country)
    .set('nationality', nationality)
    .set('type-of-visa', type)
    return this.httpClient.get<VisaCategory[]>(`check-visa-category`, {headers: headers})
  }
  
}

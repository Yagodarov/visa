import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VisaServiceType } from '../models/visa-service-type';

@Injectable({
  providedIn: 'root'
})
export class VisaServiceTypesService {

  constructor(private httpClient: HttpClient) { }

  get(country: string, nationality: string, visaType: string): Observable<VisaServiceType[]> {
    const headers = new  HttpHeaders()
    .set("country", country)
    .set('nationality', nationality)
    .set('type-of-visa', visaType)
    return this.httpClient.get<VisaServiceType[]>(`check-type-of-service`, {headers: headers})
  }
}

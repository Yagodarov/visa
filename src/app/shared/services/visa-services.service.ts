import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VisaService } from '../models/visa-service';

@Injectable({
  providedIn: 'root'
})
export class VisaServicesService {

  constructor(private httpClient: HttpClient) { }

  get(country: string, nationality: string, visaType: string, visaServiceType: string): Observable<VisaService[]> {
    const headers = new  HttpHeaders()
    .set("country", country)
    .set('nationality', nationality)
    .set('type-of-visa', visaType)
    .set('type-of-service', visaServiceType)
    return this.httpClient.get<VisaService[]>(`check-service-and-price`, {headers: headers})
  }
}

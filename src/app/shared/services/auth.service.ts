import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {User} from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(data): Observable<User> | any {
    return this.httpClient.post<User>(`accounts/login/`, data)
  }

  logout(data = {}): Observable<User> {
    return this.httpClient.post<User>(`accounts/logout/`, data)
  }
}

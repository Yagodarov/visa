import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { tap } from 'rxjs/operators'
import { environment } from '../../../environments/environment'

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      url: `${environment.apiUrl}/${request.url}`
    })

    if (this.accessToken) {
      const headers = {
        'Authorization': 'Token ' + this.accessToken
      }
      request = request.clone({
        setHeaders: headers
      })
    }

    request = request.clone({
          setHeaders: {'key': 'rrgw7uhxu578ip0;ljhhw6578[-l;nufy7656'}
    })

    return next.handle(request).pipe(
      tap(
        event => {
          //if (event instanceof HttpResponse) console.log('Server response')
        },
        err => {
          if (err instanceof HttpErrorResponse) {
            if (err.status == 401) console.error('Unauthorized')
            if (err.status == 404) console.error('Not found')
          }
        }
      )
    )

  }

  get accessToken(): string {
    return localStorage.getItem('token')
  }

}

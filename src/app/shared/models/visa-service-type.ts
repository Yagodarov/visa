export class VisaServiceType {
  id: number
  name: string
  description: string
}
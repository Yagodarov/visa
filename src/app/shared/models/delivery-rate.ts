export class DeliveryRate {
    id: number
    delivery_rate: string
    price: number
    management: string
  }

export class VisaType {
    id: number
    name: string
    flag: string
    description: string
    attention_note: string
  }
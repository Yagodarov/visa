export class VisaCategory {
    id: number
    name: string
    description: string
  }
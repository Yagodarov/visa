export class News {
    id: number
    body: string
    title: string
    date: string
    photo: string
  }
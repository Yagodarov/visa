export class VisaService {
  consular_fee: number
  cost: number
  id: number
  processing: number
  service: string
  delivery: number
}

import {createAction, props} from "@ngrx/store";
import {IVisaInfo} from "../interfaces";

export const setVisaInfo = createAction(
  '[VisaInfo] setVisaInfo',
  props<{ VisaInfo: IVisaInfo }>()
)
export const resetVisaInfo = createAction('[VisaInfo] resetVisaInfo');

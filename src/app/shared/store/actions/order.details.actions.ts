import {createAction, props} from '@ngrx/store';
import {OrderDetails} from "../interfaces";

export const setOrderDetails = createAction(
  '[OrderDetails] setValue',
  props<{OrderDetails}>());
export const resetOrderDetails = createAction('[OrderDetails] resetOrderDetails');

import {createAction, props} from '@ngrx/store';
import {VisaType} from "../../models/visa-type";
import {Nationality} from "../../models/nationality";
import {Country} from "../../models/country";
import {VisaService} from "../../models/visa-service";
import {VisaCategory} from "../../models/visa-category";
import {VisaServiceType} from "../../models/visa-service-type";
import {IVisaInfo, OrderValues} from "../interfaces";

export const setCountry = createAction(
  '[Order] setCountry',
  props<{ country: Country}>());
export const setNationality = createAction(
  '[Order] setNationality',
  props<{ nationality: Nationality}>());
export const setVisaType = createAction(
  '[Order] setVisaType',
  props<{ visa_type: VisaType}>());
export const setVisaService = createAction(
  '[Order] setVisaService',
  props<{ visa_service: VisaService}>());
export const setVisaServiceDelivery = createAction(
  '[Order] setVisaServiceDelivery',
  props<{ delivery: number}>());
export const setVisaCategory = createAction(
  '[Order] setVisaCategory',
  props<{ visa_category: VisaCategory}>());
export const setVisaServiceType = createAction(
  '[Order] setVisaServiceType',
  props<{ visa_service_type: VisaServiceType}>());
export const setVisaPrice = createAction(
  '[Order] setVisaPrice',
  props<{ visa_service_type: VisaServiceType}>());

export const setOrderValues = createAction(
  '[OrderValue] setOrderValues',
  props<{ orderValues: OrderValues }>()
)
export const reset = createAction('[Order Component] Reset');

import {createAction, props} from '@ngrx/store';

export const setStep = createAction(
  '[OrderStep] setStep',
  props<{ number: number}>());
export const reset = createAction('[OrderStep] Reset');

import {IVisaInfo} from "../interfaces";
import {createReducer, on} from "@ngrx/store";
import {reset} from "../actions/order.step.actions";
import {setVisaInfo} from "../actions/order.visa.actions";

export const initialOrderVisaInfo: IVisaInfo = new class implements IVisaInfo {
  country: number;
  id: number;
  nationality: number;
  service: number;
  type_of_service: number;
  vat_fee: number;
  visa_category: number;
  visa_type: number;
}

const _orderVisaInfoReducer = createReducer(
  initialOrderVisaInfo,
  on(setVisaInfo, (state, { VisaInfo }) => { return VisaInfo}),
  on(reset, state => initialOrderVisaInfo)
);
export function orderVisaInfoReducer(state, action) {
  return _orderVisaInfoReducer(state, action);
}

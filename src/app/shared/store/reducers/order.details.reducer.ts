import {IOrderDetails, OrderDetails} from "../interfaces";
import {createReducer, on} from "@ngrx/store";
import {setOrderDetails, resetOrderDetails} from "../actions/order.details.actions";

export const initialOrderDetails: OrderDetails = {
  id: '',
  address_main_destination: '',
  company_address: '',
  company_name: '',
  date: '',
  date_of_birth: '',
  date_passport_back: '',
  gender: '',
  job_title: '',
  main_destination: '',
  name: '',
  other_destinations: '',
  passport_expiry_date: '',
  passport_number: '',
  purpose_of_visit: '',
  surname: '',
  trips_soon: '',
  visa_end_date: '',
  visa_start_date: '',
  passport_image: '',
  user: '',
  created: '',
  submitted: '',
  collected: '',
  other_places: '',
  address: '',
  main_city: '',
  position: '',
  token: ''
}

const _orderDetailsReducer = createReducer(
  initialOrderDetails,
  on(setOrderDetails, (state, {OrderDetails}) => {
   return OrderDetails;
  }),
  on(resetOrderDetails, state => initialOrderDetails)
);
export function orderDetailsReducer(state, action) {
  return _orderDetailsReducer(state, action);
}

import {OrderStep} from "../interfaces";
import {createReducer, on} from "@ngrx/store";
import {reset, setStep} from "../actions/order.step.actions";

export const initialOrderStepState: OrderStep = {
  number: 1
}

const _orderStepReducer = createReducer(
  initialOrderStepState,
  on(setStep, (state, { number }) => ({ ...state, number: number})),
  on(reset, state => ({ number: 1}))
);
export function orderStepReducer(state, action) {
  return _orderStepReducer(state, action);
}

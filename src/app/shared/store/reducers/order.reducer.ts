import { createReducer, on } from '@ngrx/store';
import {
  setNationality,
  setVisaType,
  reset,
  setCountry,
  setVisaService,
  setVisaCategory, setVisaServiceType, setOrderValues, setVisaServiceDelivery
} from '../actions/order.actions';
import {IOrder, IOrderValues, Order, OrderValues} from "../interfaces";

export const initialOrderState: IOrder = new Order({
  delivery: 0
})

export const initialOrderValuesState: IOrderValues = new OrderValues()

const _orderReducer = createReducer(
  initialOrderState,
  on(setNationality, (state, { nationality }) => ({ ...state, nationality: nationality})),
  on(setCountry, (state, { country }) => ({ ...state, country: country})),
  on(setVisaType, (state, { visa_type }) => ({ ...state, visa_type: visa_type})),
  on(setVisaService, (state, { visa_service }) => ({ ...state, visa_service: visa_service})),
  on(setVisaServiceDelivery, (state, { delivery }) => {
    console.log(state)
    let newState = Object.assign({}, state)
    newState.delivery = delivery
    return newState;
  }),
  on(setVisaCategory, (state, { visa_category }) => ({ ...state, visa_category: visa_category})),
  on(setVisaServiceType, (state, { visa_service_type }) => ({ ...state, visa_service_type: visa_service_type})),
  on(reset, state => ({ country: null, nationality: null, visa_type: null}))
);

const _orderValuesReducer = createReducer(
  initialOrderValuesState,
  on(setOrderValues, (state, {orderValues}) => {
    return orderValues;
  }),
  on(reset, state => initialOrderValuesState)
)

export function orderValuesReducer(state, action) {
  return _orderValuesReducer(state, action);
}

export function orderReducer(state, action) {
  return _orderReducer(state, action);
}

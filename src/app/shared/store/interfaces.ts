import {VisaType} from "../models/visa-type";
import {Country} from "../models/country";
import {Nationality} from "../models/nationality";
import {VisaService} from "../models/visa-service";
import {VisaCategory} from "../models/visa-category";
import {VisaServiceType} from "../models/visa-service-type";

export interface IOrder {
  nationality: Nationality;
  country: Country;
  visa_type: VisaType;
  visa_service: VisaService;
  visa_category: VisaCategory;
  visa_service_type: VisaServiceType;
  delivery: number;
}

export class Order implements IOrder {
  nationality: Nationality;
  country: Country;
  visa_type: VisaType;
  visa_service: VisaService;
  visa_category: VisaCategory;
  visa_service_type: VisaServiceType;
  delivery: number;

  constructor(data?: Partial<IOrder>) {
    Object.assign(this, data);
  }
}

export interface IOrderValues {
  id: string;
  country: string,
  nationality: string,
  visa_type: string,
  visa_category: string,
  type_of_service: string,
  service: string
  vat_fee: string;
}

export class OrderValues implements IOrderValues {
  id: string;
  country: string;
  nationality: string;
  service: string;
  type_of_service: string;
  visa_category: string;
  visa_type: string;
  vat_fee: string;

  constructor(data?: Partial<IOrderValues>) {
    Object.assign(this, data);
  }
}

export interface OrderValuesPost {
  country: number;
  nationality: number;
  service: number;
  type_of_service: number;
  visa_category: number;
  visa_type: number;
}

export interface IOrderDelivery {
  delivery_type: number;
  delivery_rate: number;
  first_name: string;
  last_name: string;
  country: string;
  city: string;
  company: string;
  address: string;
  post_code: string;
  invoice: boolean;
  order: number
}

export interface OrderStep {
  number: number;
}

export interface IOrderDetails {
  id: string,
  name: string,
  surname: string,
  date: string,
  gender: string,
  passport_number: string,
  date_passport_back: string,
  visa_start_date: string,
  visa_end_date: string,
  date_of_birth: string,
  passport_expiry_date: string,
  purpose_of_visit: string,
  main_destination: string,
  other_destinations: string,
  address_main_destination: string,
  job_title: string,
  trips_soon: string,
  company_name: string,
  company_address: string,
  other_places: string,
  address: string,
  main_city: string,
  position: string,
  passport_image: string,
  user: string,
  created: string,
  submitted: string,
  collected: string
  token: string;
}

export class OrderDetails implements IOrderDetails {
  id: string;
  address_main_destination: string;
  company_address: string;
  company_name: string;
  date: string;
  date_of_birth: string;
  date_passport_back: string;
  gender: string;
  job_title: string;
  main_destination: string;
  name: string;
  other_destinations: string;
  passport_expiry_date: string;
  passport_number: string;
  purpose_of_visit: string;
  surname: string;
  trips_soon: string;
  visa_end_date: string;
  visa_start_date: string;
  other_places: string;
  address: string;
  main_city: string;
  position: string;
  passport_image: string;
  user: string;
  created: string;
  submitted: string;
  collected: string;
  token: string;
  constructor(data: Partial<IOrderDetails>) {
    Object.assign(this, data);
  }
}

export interface IVisaInfo {
  id: number,
  country: number,
  nationality: number,
  visa_type: number,
  visa_category: number,
  type_of_service: number,
  service: number,
  vat_fee: number
}

export class VisaInfo implements IVisaInfo {
  country: number;
  id: number;
  nationality: number;
  service: number;
  type_of_service: number;
  vat_fee: number;
  visa_category: number;
  visa_type: number;

}

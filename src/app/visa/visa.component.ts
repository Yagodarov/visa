import { Component, OnInit } from '@angular/core';
import {VisaCategoriesService} from "../shared/services/visa-categories.service";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";

@Component({
  selector: 'app-visa',
  templateUrl: './visa.component.html',
  styleUrls: ['./visa.component.scss']
})
export class VisaComponent implements OnInit {
  count$: Observable<number>
  constructor(
    protected visaService: VisaCategoriesService,
    private store: Store<{ count: number }>
  ) {
    this.count$ = store.select('count');
  }

  public ngOnInit() {

  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { MainpageComponent } from './mainpage/mainpage.component';
import { FirstBlockComponent } from './mainpage/first-block/first-block.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SecondBlockComponent } from './mainpage/second-block/second-block.component';
import { FaqService } from './shared/services/faq.service';
import { NewsService } from './shared/services/news.service';
import { AuthInterceptor } from './shared/helpers/auth.interceptor';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FaqBlockComponent } from './mainpage/faq-block/faq-block.component';
import { NewsBlockComponent } from './mainpage/news-block/news-block.component';
import { DragScrollModule } from 'ngx-drag-scroll';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SigninComponent } from './signin/signin.component';
import { VisaComponent } from './visa/visa.component';
import {StoreModule} from "@ngrx/store";
import {orderReducer, orderValuesReducer} from "./shared/store/reducers/order.reducer";
import {OrderModule} from "./order/order.module";
import {orderStepReducer} from "./shared/store/reducers/order.step.reducer";
import {orderDetailsReducer} from "./shared/store/reducers/order.details.reducer";
import {orderVisaInfoReducer} from "./shared/store/reducers/order.visa.reducer";
import { DndDirective } from './shared/directives/dnd.directive';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MainpageComponent,
    FirstBlockComponent,
    SecondBlockComponent,
    FaqBlockComponent,
    NewsBlockComponent,
    SigninComponent,
    VisaComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    MatSelectModule,
    MatFormFieldModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DragScrollModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      order: orderReducer,
      orderStep: orderStepReducer,
      orderDetails: orderDetailsReducer,
      orderValues: orderValuesReducer,
      orderVisaInfo: orderVisaInfoReducer
    }),
    OrderModule,
  ],
  providers: [
    FaqService,
    NewsService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

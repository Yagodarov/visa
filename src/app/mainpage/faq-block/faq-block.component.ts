import { Component, OnInit } from '@angular/core';
import { Faq } from 'src/app/shared/models/faq';
import { FaqService } from 'src/app/shared/services/faq.service';

@Component({
  selector: 'app-faq-block',
  templateUrl: './faq-block.component.html',
  styleUrls: ['./faq-block.component.scss']
})
export class FaqBlockComponent implements OnInit {

  public faqs: Faq[]
  public selCategory: string = 'GENERAL'
  constructor( private faqService: FaqService) { }

  ngOnInit(): void {
    this.getFaqs()
  }

  getFaqs(){
    this.faqService.get().subscribe(
      result => {
        this.faqs = result
      }
    )
  }

}

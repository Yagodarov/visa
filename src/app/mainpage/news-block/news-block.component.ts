import { Component, OnInit, ViewChild } from '@angular/core';
import { DragScrollComponent } from 'ngx-drag-scroll';
import { News } from 'src/app/shared/models/news';
import { NewsService } from 'src/app/shared/services/news.service';

@Component({
  selector: 'app-news-block',
  templateUrl: './news-block.component.html',
  styleUrls: ['./news-block.component.scss']
})
export class NewsBlockComponent implements OnInit {

  public news: News[]
  @ViewChild('nav', {read: DragScrollComponent}) ds: DragScrollComponent

  constructor( private faqService: NewsService) { }

  ngOnInit(): void {
    this.getNews()
  }

  getNews(){
    this.faqService.get().subscribe(
      result => {
        this.news = result
      }
    )
  }

  moveLeft() {
    this.ds.moveLeft();
  }
 
  moveRight() {
    this.ds.moveRight();
  }
}

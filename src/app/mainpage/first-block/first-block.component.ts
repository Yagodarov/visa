import { Component, OnInit } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { Router } from '@angular/router';
import { Country } from 'src/app/shared/models/country';
import { Nationality } from 'src/app/shared/models/nationality';
import { RootService } from 'src/app/shared/services/root.service';
import {Observable} from "rxjs";
import {IOrder} from "../../shared/store/interfaces";
import {Store} from "@ngrx/store";
import {setCountry, setNationality} from "../../shared/store/actions/order.actions";
declare var $:any;
@Component({
  selector: 'app-first-block',
  templateUrl: './first-block.component.html',
  styleUrls: ['./first-block.component.scss']
})
export class FirstBlockComponent implements OnInit {
  public order: Observable<IOrder>
  public country: string
  public nationality: string
  public countries: Country[]
  public nationalities: Nationality[]
  public selCountry: string
  public selNationality: string
  constructor(
    private rootService: RootService,
    private router: Router,
    private store: Store<{ order: IOrder }>) {
    this.order = store.select('order')
  }

  ngOnInit(): void {
    this.getCountriesAndNationalities()
  }

  getCountriesAndNationalities() {
    this.rootService.get().subscribe(
      result => {
        this.countries = result.countries
        this.nationalities = result.nationalities
      }
    )
  }

  initSelect2() {
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
  }

  goToOrder() {
    this.router.navigate(
      ['/order'],
      {
        queryParams: {
          country: this.selCountry,
          nationality: this.selNationality
        }
      }
    )
  }

  onNationalityChange() {
    this.store.dispatch(setNationality({nationality: this.nationalities.find((n) => n.name == this.nationality)}))
  }
  onCountryChange() {
    this.store.dispatch(setCountry({country: this.countries.find((c) => c.name == this.country)}))
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-block',
  templateUrl: './second-block.component.html',
  styleUrls: ['./second-block.component.scss']
})
export class SecondBlockComponent implements OnInit {
  flags = [{
      src: "russia",
      country: "Russia"
    },{
      src: "kazakhstan",
      country: "Kazakhstan"
    },{
      src: "finland",
      country: "Finland"
    },{
      src: "norway",
      country: "Norway"
    },{
      src: "ireland",
      country: "Ireland"
    },{
      src: "gb",
      country: "UK"
    },{
      src: "usa",
      country: "Usa"
    },{
      src: "chile",
      country: "Chile"
    },{
      src: "uruguay",
      country: "Uruguay"
    },{
      src: "puerto-rico",
      country: "Puerto Rico"
    },{
      src: "new-zealand",
      country: "New Zealand"
    },{
      src: "switzerland",
      country: "Switzerland"
    },
  ]
  whyuses = [
    {
      header: "Business mission",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur nostrud fugiat."
    },{
      header: "Mobile biometrics service",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur nostrud fugiat "
    },{
      header: "We check documents online",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur."
    },{
      header: "Travel around the world",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur nostrud fugiat magna cillum voluptate exercitation est."
    },{
      header: "You don’t fill out forms",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur nostrud fugiat."
    },{
      header: "Easy payment",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur."
    },{
      header: "Safe submission",
      description: "Nulla deserunt consectetur anim aliquip dolor enim officia fugiat enim consectetur officia. Cupidatat et do velit excepteur nostrud fugiat."
    },
  ]
  getIndex(i) {
    return i + 1
  }
  constructor() { }

  ngOnInit(): void {
  }

}
